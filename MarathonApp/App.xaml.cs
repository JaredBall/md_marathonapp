﻿using System;
using System.Net.Http;
using System.Reflection;
using MarathonApp.ViewModels;
using MarathonApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MarathonApp
{
    public partial class App : Application
    {
        public App()
        {

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders
                .UserAgent.ParseAdd($"MarathonApp/{Assembly.GetExecutingAssembly().GetName().Version}");
            httpClient.BaseAddress = new Uri("http://joewetzel.com/fvtc/marathon/");
            ViewModelBase.HttpClient = httpClient;
            InitializeComponent();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
