﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace MarathonApp.Models
{
    public class RaceResults
    {
        [JsonConstructor]
        public RaceResults([JsonProperty("results")] IEnumerable<RaceResult> results)
        {
            if (results != null)
            {
                Results = results.OrderBy(r => r.Placement).ToArray();
            }
            else
                Results = Array.Empty<RaceResult>();
            RaceResult? firstResult = Results.FirstOrDefault(r => r != null);
            if (firstResult != null)
            {
                ID = firstResult.ID;
            }
        }
        public int ID { get; set; }

        public readonly RaceResult[] Results;
    }
}
