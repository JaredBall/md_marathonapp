﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MarathonApp.Models
{
    public class RaceCollection
    {
        [JsonConstructor]
        public RaceCollection([JsonProperty("races")] IEnumerable<Race> races)
        {
            Races = races?.ToArray() ?? Array.Empty<Race>();
        }

        public readonly Race[] Races;
    }
}
