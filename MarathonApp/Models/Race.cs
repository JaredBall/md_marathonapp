﻿using Newtonsoft.Json;

namespace MarathonApp.Models
{
    public class Race
    {
        private string? _raceName;
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("race_name")]
        public string RaceName 
        { 
            get => _raceName ?? string.Empty; 
            set => _raceName = value; 
        }
    }
}
