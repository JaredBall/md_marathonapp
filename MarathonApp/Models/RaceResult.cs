﻿using System;
using Newtonsoft.Json;

namespace MarathonApp.Models
{
    public class RaceResult
    {
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("race_id")]
        public int RaceId { get; set; }
        [JsonProperty("placement")]
        public int Placement { get; set; }
        [JsonProperty("name")]
        public string? Winner { get; set; }
        [JsonProperty("time")]
        public TimeSpan Time { get; set; }
        [JsonProperty("state")]
        public string? State { get; set; }
        public override string ToString()
        {
            return $"{ID}: #{Placement}: {Winner} ({Time})";
        }
    }
}
