﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.IO;
using System.Net.Http;
using System.Runtime.CompilerServices;

namespace MarathonApp.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        protected static JsonSerializer Serializer = new JsonSerializer();
        public static HttpClient HttpClient = null!;

        public static T? DeserializeStream<T>(Stream stream) where T : class
        {
            using var sr = new StreamReader(stream);
            using var jstream = new JsonTextReader(sr);
            T? obj =  Serializer.Deserialize<T>(jstream);
            return obj;
        }
        public event PropertyChangedEventHandler? PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string? propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
