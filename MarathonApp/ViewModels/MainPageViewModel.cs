﻿using MarathonApp.Models;
using MVVM;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MarathonApp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel()
        {
            GetRaces = new RelayCommand(async () =>
            {
                try
                {
                    FetchingRaces = true;
                    Race[] raceArray = await GetRacesAsync();
                    RacePickerOptions.Clear();
                    foreach (Race? race in raceArray)
                    {
                        RacePickerOptions.Add(race);
                    }
                    SelectedRace = RacePickerOptions.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    string message = ex.ToString();
                }
                finally
                {
                    FetchingRaces = false;
                }
            }, () => !FetchingRaces);

            GetRaceResults = new RelayCommand<Race>(async (race) =>
            {
                try
                {
                    FetchingResults = true;
                    var results = await GetRaceResultsAsync(race);
                    RaceResults.Clear();
                    foreach (var result in results)
                    {
                        RaceResults.Add(result);
                    }
                }
                catch (Exception ex)
                {
                    string message = ex.ToString();
                }
                finally
                {
                    FetchingResults = false;
                }

            }, r => !FetchingResults && r != null);
            GetRaces.Execute();

        }

        private Race? _selectedRace;

        public Race? SelectedRace
        {
            get { return _selectedRace; }
            set
            {
                if (_selectedRace == value) return;
                _selectedRace = value;
                NotifyPropertyChanged();
                GetRaceResults.RaiseCanExecuteChanged();
            }
        }

        private bool _fetchingRaces;

        public bool FetchingRaces
        {
            get { return _fetchingRaces; }
            set
            {
                if (_fetchingRaces == value) return;
                _fetchingRaces = value;
                NotifyPropertyChanged();
                GetRaces.RaiseCanExecuteChanged();
            }
        }

        private bool _fetchingResults;

        public bool FetchingResults
        {
            get { return _fetchingResults; }
            set
            {
                if (_fetchingResults == value) return;
                _fetchingResults = value;
                NotifyPropertyChanged();
                GetRaceResults.RaiseCanExecuteChanged();
            }
        }
        private async Task<Race[]> GetRacesAsync()
        {
            System.Net.Http.HttpResponseMessage? response = await HttpClient.GetAsync("races/");
            response.EnsureSuccessStatusCode();
            using var stream = await response.Content.ReadAsStreamAsync();
            RaceCollection? raceCollection = DeserializeStream<RaceCollection>(stream);
            return raceCollection?.Races ?? Array.Empty<Race>();
        }
        private async Task<RaceResult[]> GetRaceResultsAsync(Race race)
        {
            System.Net.Http.HttpResponseMessage? response = await HttpClient.GetAsync($"results/{race.ID}");
            response.EnsureSuccessStatusCode();
            Stream stream = await response.Content.ReadAsStreamAsync();
            RaceResults? results = DeserializeStream<RaceResults>(stream);
            return results?.Results ?? Array.Empty<RaceResult>();
        }

        public RelayCommand GetRaces { get; }
        public RelayCommand<Race> GetRaceResults { get; }

        public ObservableCollection<Race> RacePickerOptions { get; } = new ObservableCollection<Race>();

        public ObservableCollection<RaceResult> RaceResults { get; } = new ObservableCollection<RaceResult>();
    }
}
